/// <reference path="babylon.d.ts" />

var app = app || {};

app.init = function(){
    //get the canvas
    var canvas = document.getElementById('renderCanvas');

    //create a BabylonJS engine object, true for antialias
    var engine = new BABYLON.Engine(canvas, true);

    BABYLON.SceneLoader.Load("assets/images/", "robot.babylon", engine, function (scene) {
        var gl = new BABYLON.GlowLayer("glow", scene);
        gl.customEmissiveColorSelector = function(mesh, subMesh, material, result) {
            if ((mesh.name === "stars") || (mesh.name === "star")) {
                result.set(1, 1, 1, 1);
            } else {
                result.set(0, 0, 0, 0);
            }
        }
        scene.executeWhenReady(function () {
        
            // Attach camera to canvas inputs
            scene.activeCamera.attachControl(canvas);
            scene.activeCamera.position.x += 1;
            
            addFireAnimation(scene);
            addRobots(scene);
            addLights(scene);
            addNightSkyBox(scene);
            addStars(scene);
            var stars = scene.getMeshByName("stars");
            console.log(stars);
            // stars.dispose();
            var count = 0;
            engine.runRenderLoop(function () {
                if (scene.animationGroups.length > 1 && !!window.triggerAnimation) {
                    console.log(scene.animationGroups);
                    scene.getAnimationGroupByName("PointingRobot").start(true);
                } else if(scene.animationGroups.length > 1 && !window.triggerAnimation) {
                    scene.getAnimationGroupByName("PointingRobot").stop();
                }
                count++;
                gl.intensity = 0.3 * Math.sin(count / 90 * Math.PI) + 0.7;
                if (count > 360) {
                    count = 0;
                }
                scene.render();
            });
        });
    });


};

function addFireAnimation(scene) {
    var fire1 = scene.getMeshByName("fire_LP");
    fire1.dispose();

    BABYLON.SceneLoader.ImportMesh("", "assets/images/", "Fire_T_Glow.glb", scene, function (meshes) {
        meshes.forEach(item => {
            item.scaling = new BABYLON.Vector3(20, 20, 20);
            item.position = new BABYLON.Vector3(-1.5, 0, -0.2);
        });
    });
}

function addRobots(scene) {
    var robots = scene.getMeshByName("robots");
    robots.dispose();
    BABYLON.SceneLoader.ImportMesh("", "assets/images/", "Robot_Cliff_Animation_V2.glb", scene, function (meshes) {
        meshes.forEach(item => {
            item.scaling = new BABYLON.Vector3(-10, 10, 10);
            item.position = new BABYLON.Vector3(-1.8, 0, 0.5);
        });
    });
    BABYLON.SceneLoader.ImportMesh("", "assets/images/", "TwoRobots_Cliff_v2.glb", scene, function (meshes) {
        meshes.forEach(item => {

            item.scaling = new BABYLON.Vector3(-3.15, 3.15, 3.15);
            item.position = new BABYLON.Vector3(-0.40, 0, -0.44);
            item.rotate(new BABYLON.Vector3(0, 1, 0), 1 * Math.PI / 2, BABYLON.Space.LOCAL);
        });
    });
}

function addLights(scene) {
    var light1 = new BABYLON.PointLight("fireLight",  new BABYLON.Vector3(-1.5, 0.4, -0.2), scene);
    light1.diffuse = new BABYLON.Color3(1.0, 0.5, 0.0);
    light1.intensity = 50;
    // var light2 = new BABYLON.DirectionalLight("DirectionalLight", new BABYLON.Vector3(0, -2, 0), scene);
    var light3 = new BABYLON.HemisphericLight("HemisphericLight", new BABYLON.Vector3(0, 2, -5), scene);
}

function addNightSkyBox(scene) {
    var skybox = BABYLON.MeshBuilder.CreateBox("skyBox", {size:5000}, scene);
    var skyboxMaterial = new BABYLON.StandardMaterial("skyBox", scene);
    skyboxMaterial.backFaceCulling = false;
    var files = [
        "assets/textures/Space2/space_left.jpg",
        "assets/textures/Space2/space_up.jpg",
        "assets/textures/Space2/space_front.jpg",
        "assets/textures/Space2/space_right.jpg",
        "assets/textures/Space2/space_down.jpg",
        "assets/textures/Space2/space_back.jpg",
    ];
    skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture.CreateFromImages(files, scene);
    skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
    skyboxMaterial.disableLighting = true;
    skybox.material = skyboxMaterial;
    skybox.rotation.x = Math.PI / 2;
    skybox.rotation.y = - Math.PI / 2;
    skybox.rotation.z = Math.PI / 2;
    // stars.material.emissiveColor = new BABYLON.Color3(1, 1, 1);
}

function addStars(scene) {
    BABYLON.SceneLoader.ImportMesh("", "assets/images/", "Star.glb", scene, function (meshes) {
        var star = meshes[0];
        for (var count = 0; count < 1000; count++) {
            var offsetX = 200 * Math.random() - 100;
            var offsetY = 200 * Math.random() - 100;
            var offsetZ = 200 * Math.random() - 100;
            for (index = 1; index < meshes.length; index++) {
                meshes[index].scaling = new BABYLON.Vector3(1, 1, 1);
                meshes[index].position = new BABYLON.Vector3(100, 100, 100);
                var instance = meshes[index].createInstance("instance" + count);
                instance.parent = meshes[index].parent;

                if (!instance.parent.subMeshes) {
                    instance.position.x =  0 + offsetX;
                    instance.position.z =  0 + offsetZ;
                    instance.position.y = 100 + offsetY;
                }
            }
        }
    });
}