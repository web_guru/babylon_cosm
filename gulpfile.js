const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');

// .scss > .css
gulp.task('sass', ()=>
    gulp.src('src/sass/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('public/assets/css'))
);

gulp.task('watch', function () {
    gulp.watch([
        'src/sass/*.scss',
        'src/sass/**/*.scss',
    ], gulp.series(['sass',]));

});

/* 
    The default task
*/
gulp.task('default', gulp.series('sass', 'watch'));